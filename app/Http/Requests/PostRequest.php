<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\RequiredIf;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function message()
    {
        return [
            // Required validations
            'title.required' => 'Title must not be empty',
            'content.required' => 'Content must not be empty',
            'published_at.required' => 'Published at must not be empty',
            'status.required' => 'Status must not be empty',
            'slug.required' => 'Slug must not be empty',

            //Conditional Validations
            'published_at.after' => 'A new post cannot be published in the past'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
            case 'PUT':
            case 'PATCH':
                $rules = [
                    'title' => 'required',
                    'content' => 'required',
                    'published_at' => 'required_unless:status,draft',
                    'status' => 'required',
                    'slug' => 'required|alpha-dash|unique:posts,slug'
                ];

                if ($this->status != 'draft') {
                    $rules['published_at'] = 'required|date|after:today';
                }

                return $rules;
       }
    }
}
