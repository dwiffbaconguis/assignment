<?php

namespace App\Models\Post;

use Carbon\Carbon;
use App\Models\Category\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'title','slug','content','status','published_at'
    ];

    /**
     * The attributes that should be cast.
     * @var array
     */
    protected $casts = [
        'published_at'  => 'datetime',
    ];

    public function getAllPosts()
    {
        return $this->orderBy('created_at', 'DESC')->get();
    }

    public function createPost($attributes)
    {
        $post = $this->create($attributes);

        return $post;
    }

    public function updatePost($attributes) : Post
    {
        $post = $this->fill($attributes);
        $post->save();

        return $post;
    }

    public function updateAllPost() : void
    {
        foreach ($this->all() as $post) {
            $publishDate = $post->published_at;

            if ($publishDate < Carbon::now()) {
                $status = 'published';
            }

            if ($publishDate == null) {
                continue;
            }

            if ($post->status === 'draft') {
                $publishDate = null;
            }

            $post->fill([
                'title' => $post->title,
                'content' => $post->content,
                'slug' => $post->slug,
                'status' => $status,
                'published_at' => $publishDate
            ]);

            $post->save();
        }
    }

    /**
     * The categories that belong to the post.
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }
}
