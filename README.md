# Tussendoor - Laravel Assignment
---

Laravel Assignment Changes (Started: 9:00pm (04/09/2021) PST)

## Code changes
---

- There is almost no input validation
    - Creates a Form Request to handle all the input validations and other conditional validations such as status and published_at.
    - I spent most of my time figuring out the better and clean approach for implementing validations, there are tons of conditions that needs to be refactored
    and should be cleaned. So what I did is I gather all the necessary rules needed for the input fields and define their respective messages.
    - I also create a switch case in PostRequest to handle multiple methods, but later did I figure out that the rules that they need is the same.
- Code is repeated throughout the controller and violates accepted design principles (DRY, encapsulation, delegation)
    - Controllers should not be thinking, as the name itself. So conditional statements, queries, validations and etc. will clearly violate the Single Responsibility Principle of the Controller.
    - Refactored all repetitive codes I could see, maximized the Post model on processing the requests.
    - Applied also the design principles for better, clean and readable code.
- In some blade views presentation is mixed with code
    - Haven't reached in blade views, my time runs out on back-end.
- Some builtin Laravel functionality is not utilized
    - Yes, specially in setting input validations.
    - Using Form Request
    - Using validated() request in Form Request to eliminate instantiation of Model.
    - Model, also is not properly utilized.
- Slugs are not automatically created nor are they checked for duplicates
    - I implemented automatic conversion of string to slug but encountered an error.
    - Converted the slug field on back-end before saving using Str::slug and it was successful, however, when I entered the same string on slug field, it still get's accepted
    because it already passed the validation 'required|unique:posts, slug'.
    - So instead of using the 2nd approach, I added a validation for the slug field to use alpha-dash. So now, users will be prompted that the slug field should be in alpha-dash format.